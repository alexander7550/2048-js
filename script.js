import './style.css';

let game = document.getElementById("game");
let size = 4;

let isGameOver = false;
let score = 0;

let container = document.querySelector("#game");

container.addEventListener("touchstart", startTouch, false);
container.addEventListener("touchmove", moveTouch, false);

// Swipe Up / Down / Left / Right
var initialX = null;
var initialY = null;

for(let x = size; x > 0; x--)
{
    for (let y = 1; y <= size; y++)
    {
        let coords = `c${y}-${x}`;

        let div_container = document.createElement("div");
        div_container.classList.add("plitka-container");
        div_container.classList.add("border");

        let div_id = document.createElement("div");
        div_id.id = coords;
        div_id.innerHTML = "&nbsp;";

        div_id.setAttribute("data-value", "0");

        div_container.appendChild(div_id);

        game.appendChild(div_container);
    }
}
let types = [0 ,2, 4, 8, 16, 32, 64, 128, 256]

PlaceRandomPlitka();
PlaceRandomPlitka();

function PlaceRandomPlitka(){  
    let c = getRandomCoords();
    let value = getRandom(1,3);

    SetPlitka({ x: c.x, y: c.y, value: types[value] });
}

document.addEventListener("keydown", (e) =>
{
    switch(e.key)
    {   
        case "ArrowUp":
        case "ArrowDown":
        case "ArrowRight":
        case "ArrowLeft":
            keyDownEvent(e.key);
            e.preventDefault();
            break;
    }
});

function keyDownEvent(key) {
    if (isGameOver) return;

    let changes = 0;

    switch(key) {
        case "ArrowUp": 
            changes = ReDraw("up");
        
            break;
        case "ArrowDown":            
            changes = ReDraw("down");
            
            console.log("изменений вниз: " + changes);

            break;
        case "ArrowRight":
            changes = ReDraw("right");

            break;
        case "ArrowLeft":
            changes = ReDraw("left");

            break;
        default: break;
    }      
    
    if (changes > 0)
    {
        PlaceRandomPlitka(); 
        let ccccc = getRandomCoords();
        if (ccccc.x == -1 && ccccc.y == -1) // некуда воткнуть ещё одну новую плашку
        {
            if (CalcPossibleCombines() == 0) // нельзя ничего объединить на текущем поле
            {
                alert(`Игра окончена. Ваш результат ${score}.`);
                isGameOver = true;
                return;
            }
        }
    }
}

function startTouch(e) {
    initialX = e.touches[0].clientX;
    initialY = e.touches[0].clientY;
}

function moveTouch(e) {
    if (initialX === null) {
      return;
    }

    if (initialY === null) {
      return;
    }

    var currentX = e.touches[0].clientX;
    var currentY = e.touches[0].clientY;

    var diffX = initialX - currentX;
    var diffY = initialY - currentY;

    if (Math.abs(diffX) < 50 && Math.abs(diffY) < 50)
    {
        e.preventDefault();
        console.log("too low");
        return;
    }

    if (Math.abs(diffX) > Math.abs(diffY)) {
      // sliding horizontally
      if (diffX > 0) {
        // swiped left
        keyDownEvent("ArrowLeft");
      } else {
        // swiped right
        keyDownEvent("ArrowRight");
      }  
    } else {
      // sliding vertically
      if (diffY > 0) {
        // swiped up
        keyDownEvent("ArrowUp");
      } else {
        // swiped down
        keyDownEvent("ArrowDown");
      }  
    }

    initialX = null;
    initialY = null;

    e.preventDefault();
}

function ReDraw(direction)
{
    let changed = 0;

    switch(direction)
    {
        case "left": 
            for(let y = size; y > 0; y--)
            {
                changed +=moveLeft(y);

                if ( CombinePlitki(1, y, "left", false) ) changed++;
                if ( CombinePlitki(2, y, "left", false) ) changed++;
                if ( CombinePlitki(3, y, "left", false) ) changed++;

                changed +=moveLeft(y);                
            }
            break;
        
        case "right":
            for(let y = size; y > 0; y--)
            {
                changed +=moveRight(y);

                if ( CombinePlitki(4, y, "right", false) ) changed++;
                if ( CombinePlitki(3, y, "right", false) ) changed++; 
                if ( CombinePlitki(2, y, "right", false) ) changed++;

                changed +=moveRight(y);
            }
            break;

        case "up":
            for(let x = 1; x <= size; x++)
            {
                changed += moveUp(x);
                
                if ( CombinePlitki(x, 4, "up", false) ) changed++; 
                if ( CombinePlitki(x, 3, "up", false) ) changed++;
                if ( CombinePlitki(x, 2, "up", false) ) changed++;

                changed +=moveUp(x);
            }
            break;

        case "down":
            for(let x = size; x > 0; x--)
            {
                changed += moveDown(x);

                if ( CombinePlitki(x, 1, "down", false) ) changed++;
                if ( CombinePlitki(x, 2, "down", false) ) changed++;
                if ( CombinePlitki(x, 3, "down", false) ) changed++;

                changed += moveDown(x);
            }
            break;

        default: break;
    }

    return changed;
}

function CalcPossibleCombines() {
    let possible_combines = 0;
    for(let y = 1; y <= size; y++)
    {
        let two_have_value = false;
        let three_have_value = false;
        for(let x = 1; x < size; x++)
        {
            let one = GetPlitkaValue( { x:x,   y:y } );
            let two = GetPlitkaValue( { x:x+1, y:y } );

            if (x == 2 && one > 0) two_have_value = true; 
            if (x == 3 && one > 0) three_have_value = true;
                
            if (one > 0 && two > 0 && one == two) possible_combines++;
        }

        if (!two_have_value && !three_have_value)
        {
            let one = GetPlitkaValue( { x:1,   y:y } );
            let two = GetPlitkaValue( { x:4, y:y } );

            if (one > 0 && two > 0 && one == two) possible_combines++;
        }
    }

    for(let x = 1; x <= size; x++)
    {
        let two_have_value = false;
        let three_have_value = false;
        for(let y = 1; y < size; y++)
        {
            let one = GetPlitkaValue( { x:x, y:y } );
            let two = GetPlitkaValue( { x:x, y:y+1 } );

            if (y == 2 && one > 0) two_have_value = true; 
            if (y == 3 && one > 0) three_have_value = true;

            if (one > 0 && two > 0 && one == two) possible_combines++;
        }

        if (!two_have_value && !three_have_value)
        {
            let one = GetPlitkaValue( { x:x, y:1 } );
            let two = GetPlitkaValue( { x:x, y:4 } );

            if (one > 0 && two > 0 && one == two) possible_combines++;
        }

    }
    console.log(`Возможно различных объединений: ${possible_combines}.`);

    return possible_combines;
}



function CombinePlitki(x, y, type, only_calc) {
    let xx = x;
    let yy = y;

    switch(type)
    {
        case "left":
            xx = x + 1;
            break;
        case "right":
            xx = x - 1;
            break;
        case "up":
            yy = y - 1;
            break;
        case "down":
            yy = y + 1;
            break;
    }

    let one = GetPlitkaValue( { x: x, y: y } );
    let two = GetPlitkaValue( { x: xx, y: yy } );

    if(one > 0 && two > 0 && one == two) {
        if (only_calc) return true;
        ClearPlitka( { x: x, y: y } );
        ClearPlitka( { x: xx, y: yy } );
        SetPlitka( { x: x, y: y, value: one*2 } );
        score += one*2;
        setScore(score);

        return true;
    }

    return false;
}

function moveLeft(y) {
    let changed = 0;
    for(let x = 1; x <= size; x++)
    {
        let b = moveIt(x, y, "left");
        if (b) changed++;
    }
    return changed;   
}
function moveRight(y)
{
    let changed = 0;
    for(let x = size; x > 0; x--)
    {
        let b = moveIt(x, y, "right");
        if (b) changed++;
    }
    return changed;   
}
function moveUp(x)
{
    let changed = 0;
    for(let y = size; y > 0; y--)
    {
        let b = moveIt(x, y, "up");
        if (b) changed++;
    }
    return changed;   
}
function moveDown(x)
{
    let changed = 0;
    for(let y = 1; y <= size; y++)
    {
        let b = moveIt(x, y, "down");
        if (b) changed++;
    }
    return changed;        
}

function moveIt(x, y, direction)
{
    let value = GetPlitkaValue( { x: x, y: y } );
    if (value != "0") {
        let change_x = 0;
        let change_y = 0;

        switch (direction) {
            case "left": 
                for(let f = 1; f < x; f++) { if (GetPlitkaValue( { x: f, y: y } ) == "0") change_x++; }
                break;
            case "right":
                for(let f = x; f <= size; f++) { if (GetPlitkaValue( { x: f, y: y } ) == "0") change_x--; }
                break;
            case "up":
                for(let f = size; f > y; f--) { if (GetPlitkaValue( { x: x, y: f } ) == "0") change_y--; } 
                break;
            case "down":
                for(let f = 1; f < y; f++) { if (GetPlitkaValue( { x: x, y: f } ) == "0") change_y++; } 
                break;
        }
           
        if (change_x == 0 && change_y == 0) return false; 

        let x_new = x - change_x;
        let y_new = y - change_y;
            
        ClearPlitka({ x: x, y: y });
        SetPlitka({ x: x_new, y: y_new, value: value});

        return true;
    }
    return false;
}

function getRandom(min, max) {
    return Math.trunc(Math.random() * (max - min) + min);
}

function setScore(score){
    let div = document.getElementById("score");
    div.innerText = score;
}

function SetPlitka({x, y, value}={})
{
    let class_name = `c${x}-${y}`;
    let div = document.getElementById(class_name);
    div.classList.add("plitka"+value);
    div.innerText = value;
    div.setAttribute("data-value", value);
}
function ClearPlitka({x, y}={})
{
    let class_name = `c${x}-${y}`;
    let div = document.getElementById(class_name);
    div.className = "";
    div.innerHTML = "&nbsp;";
    div.setAttribute("data-value", "0");
}
function GetPlitkaValue({x, y}={})
{
    let class_name = `c${x}-${y}`;
    let div = document.getElementById(class_name);
    return div.getAttribute("data-value");
}

function getRandomCoords() {
    let combinations = [];

    let index = 0;
    for(let x = 1; x <= size; x++)
    {
        for(let y = 1; y <= size; y++)
        {
            combinations[index] = { x: x, y: y };
            index++;
        }
    }

    for(let t = 0; t < size*size; t++)
    {
        let rnd_index = getRandom(0, combinations.length);
        let x = combinations[rnd_index].x;
        let y = combinations[rnd_index].y;

        console.log(`[${t} try] random coods: x = ${x}, y=${y}`);

        if ( GetPlitkaValue( { x: x, y: y }) == "0" )
        {
            return { x: x, y: y };
        }
        else{
            combinations.splice(rnd_index, 1);
        } 
    }
    return { x: -1, y: -1 }
}

function getArray(from, to)
{
    let array = [];
    for (let i = from; i <= to; i++) {
        array.push(i);
    }
    return array;
}